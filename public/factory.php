<?php

use Pokedex\Contracts\Factories\PokemonViewFactory;
use Pokedex\Factories\RawPokemonViewFactory;

return [
    PokemonViewFactory::class => RawPokemonViewFactory::class,
];