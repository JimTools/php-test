$(function() {

    var page = 1;

    var totalPages =1;

    /**
     * handles the pagination request
     * @param page
     */
    function getList(page) {
        $('#list li').remove();
        $.get('/api.php', {page: page}, function(resp) {

            totalPages = resp.meta.page_total;

            for (var item of resp.data) {
                var node = $($('#list-item').html().trim());
                node.find('a').data('id', item.id).text(item.name);
                node.appendTo('#list');
            }
        });
    }

    /**
     * handles the search request
     * @param term
     */
    function search(term) {
        $('#list li').remove();
        $.get('/api.php', {term: term}, function(resp) {
            for (var item of resp.data) {
                var node = $($('#list-item').html().trim());
                node.find('a').data('id', item.id).text(item.name);
                node.appendTo('#list');
            }
        });
    }

    /**
     * handles single pokemon request
     * @param id
     */
    function getPokemon(id) {
        $('#list li').remove();
        $.get('api.php', {id: id}, function(resp) {
            var template = $($('#single').html().trim());
            var div = template.find('div');

            // sets name and species
            var strong = div.find('p > strong');
            $(strong[0]).text(resp.name);
            $(strong[1]).text(resp.species);

            // sets height and weight
            var p = div.find('p > em');
            $(p[0]).text((resp.height/10).toFixed(2) + 'm');
            $(p[1]).text((resp.weight/10).toFixed(2) + 'Kg');

            // sets abilities
            (resp.abilities || []).map(function(ability) {
                div.find('p.abilities').append($('<span>').text(ability.name));
            });

            // sets types
            (resp.types || []).map(function(type) {
                div.find('p.types').append($('<span>').text(type.name));
            });

            // image
            template.find('img').attr({src: resp.image, alt: resp.name});


            template.appendTo('#list');

        });
    }

    /**
     * resets the page controls
     */
    function resetControls() {
        $('#nav-back, #nav-next').css('display', 'inline');
        $('#back').css('display', 'none');
    }

    // initial page, loads the data for page list
    getList(page);

    // search bar function
    $('#search').on('keypress', function(e) {
        if (e.keyCode == 13) {
            resetControls();
            search($(e.target).val());
        }
    });

    // back to list
    $('#back').on('click', function(e) {
        e.preventDefault();
        resetControls();
        getList(page);
    });

    // navigation back link
    $('#nav-back').on('click', function(e) {
        e.preventDefault();
        if(page-1 > 0) {
            getList(--page);
        }
    });

    // navigation forward link
    $('#nav-next').on('click', function(e) {
        e.preventDefault();
        if(page+1 < totalPages) {
            getList(++page);
        }
    });

    // list item link
    $(document).on('click', '.item-link', function(e) {
        $('#nav-back, #nav-next').css('display', 'none');
        $('#back').css('display', 'inline');

        getPokemon($(e.target).data('id'));

    });



});