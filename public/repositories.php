<?php
use Pokedex\Contracts\Repositories\PokemonRepository;
use Pokedex\Repositories\GuzzlePokemonRepository;

return [
    PokemonRepository::class => GuzzlePokemonRepository::class
];