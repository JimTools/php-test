<?php

use Pokedex\PokedexApi;

require_once __DIR__ . '/../vendor/autoload.php';

$repositories = require './repositories.php';
$factories = require './factory.php';

// parameter cleaning
$id = intval($_GET['id']);

$term = empty(trim($_GET['term'])) ? null : trim($_GET['term']);

$page = intval($_GET['page']);
$page = $page ? $page : 1;

$limit = intval($_GET['limit']);
$limit = $limit ? $limit : 10;

try {

    $api = new PokedexApi($repositories, $factories);

    header('content-type: application/json');

    // get single result
    if(!empty($id)) {

        $found = $api->show($id);
        // not found
        if (is_array($found)) {
            header('content-type: application/json', 404, true);
            exit(json_encode($found));
        }

        exit(json_encode($found));

    }

    // gets search results
    if(!is_null($term)) {
        exit(json_encode($api->search($term)));
    }

    // returns list of pokemon
    exit(json_encode($api->index($page, $limit)));



} catch (Exception $e) {


    header('content-type: application/json', true, 500);
    exit(json_encode([
        'message' => 'Internal Server Error',
        'error' => [
            'message' => $e->getMessage(),
            'trace' => $e->getTrace(),
        ]
    ]));
}
