<?php

namespace Mocks\Repositories;

use Pokedex\Contracts\Repositories\PokemonRepository;
use Pokedex\Entities\Pokemon;
use Pokedex\Entities\Type;

class MockPokemonRepository implements PokemonRepository
{

    /**
     * @var Pokemon[]
     */
    protected $data = [];

    public function __construct()
    {
        $this->data = [
            1 => new Pokemon(1, 'bulbasuar', 'bulbasuar', 69, 7, [], [new Type(4, 'poison'), new Type(12, 'grass')], ''),
            2 => new Pokemon(2, 'ivysuar', 'ivysuar', 130, 10, [], [new Type(4, 'poison'), new Type(12, 'grass')], ''),
            3 => new Pokemon(3, 'venusaur', 'venusaur', 130, 10, [], [new Type(4, 'poison'), new Type(12, 'grass')], ''),
            4 => new Pokemon(4, 'charmander', 'charmander', 85, 6, [], [new Type(10, 'fire')], ''),
            5 => new Pokemon(5, 'charmeleon', 'charmeleon', 190, 11, [], [new Type(10, 'fire')], ''),
            6 => new Pokemon(6, 'charizard', 'charizard', 905, 17, [], [new Type(3, 'flying'), new Type(10, 'fire')], ''),
        ];
    }

    /**
     * returns a list of pokemon based off limit and ofset
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPaginated(int $limit, int $offset): array
    {;
        return array_slice($this->data, $offset, $limit);
    }

    /**
     * return a single pokemon
     *
     * @param int $id
     * @return Pokemon
     */
    public function getPokemon(int $id): ?Pokemon
    {
        if (!array_key_exists($id, $this->data)) {
            return null;
        }

        return $this->data[$id];
    }

    /**
     * search array of pokemon by search term
     *
     * @param string $term
     * @return Pokemon[]
     */
    public function findBySearch(string $term): array
    {
        $matches = [];

        foreach($this->data as $pokemon) {
            similar_text($pokemon->getName(), $term, $percent);
            if($percent > 50) {
                $matches[] = $pokemon;
            }
        }

        return $matches;
    }

    /**
     * returns the total number of pokemon
     *
     * @return int
     */
    public function getCount(): int
    {
        return count($this->data);
    }
}