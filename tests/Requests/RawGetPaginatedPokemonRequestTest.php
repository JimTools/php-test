<?php

namespace Tests\Requests;

use PHPUnit\Framework\TestCase;
use Pokedex\Requests\RawGetPaginatedPokemonRequest;

class RawGetPaginatedPokemonRequestTest extends TestCase
{

    public function testConstructor()
    {
        $request = new RawGetPaginatedPokemonRequest(2, 5);

        $this->assertEquals(2, $request->getPage());
        $this->assertEquals(5, $request->getLimit());
    }

    public function testPageSetter() {
        $request = new RawGetPaginatedPokemonRequest();
        $request->setPage(2);
        $this->assertEquals(2, $request->getPage());
    }

    public function testLimitSetter() {
        $request = new RawGetPaginatedPokemonRequest();
        $request->setLimit(5);
        $this->assertEquals(5, $request->getLimit());
    }

}