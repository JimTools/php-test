<?php

namespace Tests\Requests;

use PHPUnit\Framework\TestCase;
use Pokedex\Requests\RawSearchPokemonByNameRequest;

class RawSearchPokemonByNameRequestTest extends TestCase
{

    public function testConstructor() {

        $request = new RawSearchPokemonByNameRequest('term');
        $this->assertEquals('term', $request->getTerm());
    }

    public function testTermSetter()
    {
        $request = new RawSearchPokemonByNameRequest('term');
        $request->setTerm('other');

        $this->assertEquals('other', $request->getTerm());
    }

}