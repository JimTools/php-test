<?php

namespace Tests\Requests;

use PHPUnit\Framework\TestCase;
use Pokedex\Requests\RawGetPokemonByIdRequest;

class RawGetPokemonByIdRequestTest extends TestCase
{

    public function testConstructor() {
        $request = new RawGetPokemonByIdRequest(1);
        $this->assertEquals(1, $request->getId());
    }

    public function testIdSetter() {
        $request = new RawGetPokemonByIdRequest(2);
        $this->assertEquals(2, $request->getId());
    }
}