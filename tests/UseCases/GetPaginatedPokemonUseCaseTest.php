<?php

namespace Tests\UseCases;

use Mocks\Repositories\MockPokemonRepository;
use PHPUnit\Framework\TestCase;
use Pokedex\Factories\RawPokemonViewFactory;
use Pokedex\Requests\RawGetPaginatedPokemonRequest;
use Pokedex\Responses\RawGetPaginatedPokemonResponse;
use Pokedex\UseCases\GetPaginatedPokemonUseCase;

class GetPaginatedPokemonUseCaseTest extends TestCase
{


    public function testUseCasePaginationPage1()
    {

        $useCase = new GetPaginatedPokemonUseCase(new MockPokemonRepository(), new RawPokemonViewFactory());

        $response = new RawGetPaginatedPokemonResponse();
        $useCase->process(new RawGetPaginatedPokemonRequest(1, 2), $response);

        $this->assertEquals(1, $response->getPage());
        $this->assertEquals(3, $response->getTotalPages());
        $this->assertEquals(6, $response->getTotalItems());

        $this->assertEquals(2, count($response->getResults()));
        $this->assertEquals('bulbasuar', $response->getResults()[0]->name);
    }

    public function testUseCasePaginationPage2()
    {
        $useCase = new GetPaginatedPokemonUseCase(new MockPokemonRepository(), new RawPokemonViewFactory());
        $response = new RawGetPaginatedPokemonResponse();

        $useCase->process(new RawGetPaginatedPokemonRequest(2, 2), $response);

        $this->assertEquals(2, $response->getPage());
        $this->assertEquals('venusaur', $response->getResults()[0]->name);
    }


}
