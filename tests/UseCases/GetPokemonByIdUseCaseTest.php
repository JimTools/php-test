<?php

namespace Tests\UseCases;

use Mocks\Repositories\MockPokemonRepository;
use PHPUnit\Framework\TestCase;
use Pokedex\Factories\RawPokemonViewFactory;
use Pokedex\Requests\RawGetPokemonByIdRequest;
use Pokedex\Responses\RawGetPokemonByIdResponse;
use Pokedex\UseCases\GetPokemonByIdUseCase;

class GetPokemonByIdUseCaseTest extends TestCase
{

    public function testUseCaseGetPokemon()
    {

        $response = new RawGetPokemonByIdResponse();
        $useCase = new GetPokemonByIdUseCase(
            new MockPokemonRepository(),
            new RawPokemonViewFactory()
        );

        $useCase->process(new RawGetPokemonByIdRequest(1), $response);


        $this->assertNull($response->getMessage());
        $this->assertEquals(1, $response->getPokemon()->id);
        $this->assertEquals('bulbasuar', $response->getPokemon()->name);
    }

    public function testNotFound()
    {
        $response = new RawGetPokemonByIdResponse();
        $useCase = new GetPokemonByIdUseCase(
            new MockPokemonRepository(),
            new RawPokemonViewFactory()
        );

        $useCase->process(new RawGetPokemonByIdRequest(999), $response);

        $this->assertNull($response->getPokemon());
        $this->assertEquals($response->getMessage(), 'Unable to find pokemon.');
    }



}
