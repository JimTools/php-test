<?php

namespace Tests\UseCases;

use Mocks\Repositories\MockPokemonRepository;
use PHPUnit\Framework\TestCase;
use Pokedex\Factories\RawPokemonViewFactory;
use Pokedex\Requests\RawSearchPokemonByNameRequest;
use Pokedex\Responses\RawSearchPokemonByNameResponse;
use Pokedex\UseCases\SearchPokemonByNameUseCase;

class SearchPokemonByNameUseCaseTest extends TestCase
{
    public function testFoundSomeResults()
    {
        $useCase = new SearchPokemonByNameUseCase(new MockPokemonRepository(), new RawPokemonViewFactory());

        $response = new RawSearchPokemonByNameResponse();
        $useCase->process(new RawSearchPokemonByNameRequest('char'), $response);

        $this->assertNotEmpty($response->getResults());
        $this->assertEquals(3, $response->getCount());
    }

    public function testForNoResults() {
        $useCase = new SearchPokemonByNameUseCase(new MockPokemonRepository(), new RawPokemonViewFactory());

        $response = new RawSearchPokemonByNameResponse();
        $useCase->process(new RawSearchPokemonByNameRequest('not found'), $response);

        $this->assertEmpty($response->getResults());
        $this->assertEquals(0, $response->getCount());

    }
}