<?php

namespace Tests\Responses;

use PHPUnit\Framework\TestCase;
use Pokedex\Contracts\Views\PokemonView;
use Pokedex\Responses\RawGetPokemonByIdResponse;
use Pokedex\Views\RawPokemonView;

class RawGetPokemonByIdResponseTest extends TestCase
{

    public function testMessageSetter()
    {
        $response = new RawGetPokemonByIdResponse();
        $this->assertNull($response->getMessage());

        $response->setMessage('a message');
        $this->assertEquals('a message', $response->getMessage());
    }

    public function testPokemonSetter()
    {
        $response = new RawGetPokemonByIdResponse();
        $this->assertNull($response->getPokemon());

        $response->setPokemon(new RawPokemonView());
        $this->assertInstanceOf(PokemonView::class, new RawPokemonView());
    }

}
