<?php

namespace Tests\Responses;


use PHPUnit\Framework\TestCase;
use Pokedex\Responses\RawSearchPokemonByNameResponse;
use Pokedex\Views\RawPokemonView;

class RawSearchPokemonByNameResponseTest extends TestCase
{

    public function testDefaults()
    {
        $response = new RawSearchPokemonByNameResponse();
        $this->assertEquals([], $response->getResults());
        $this->assertEquals(0, $response->getCount());
    }

    public function testResultsSetter()
    {
        $response = new RawSearchPokemonByNameResponse();
        $this->assertEquals([], $response->getResults());

        $response->setResults([new RawPokemonView()]);

        $this->assertNotEmpty($response->getResults());
        $this->assertInstanceOf(RawPokemonView::class, $response->getResults()[0]);
    }

    public function testCountSetter() {
        $response = new RawSearchPokemonByNameResponse();
        $this->assertEquals(0, $response->getCount());

        $response->setCount(10);
        $this->assertEquals(10, $response->getCount());
    }

}