<?php

namespace Tests\Responses;

use PHPUnit\Framework\TestCase;
use Pokedex\Responses\RawGetPaginatedPokemonResponse;
use Pokedex\Views\RawPokemonView;

class RawGetPaginatedPokemonResponseTest extends TestCase
{
    public function testResultsSetter()
    {
        $response = new RawGetPaginatedPokemonResponse();
        $this->assertEquals([], $response->getResults());

        $view = new RawPokemonView();
        $view->id = 1;
        $view->name ='bulbasaur';

        $response->setResults([$view]);
        $this->assertEquals([$view], $response->getResults());

    }

    public function testTotalItemsSetter() {
        $response = new RawGetPaginatedPokemonResponse();
        $this->assertEquals(0, $response->getTotalItems());

        $response->setTotalItems(10);
        $this->assertEquals(10, $response->getTotalItems());
    }

    public function testTotalPagesSetter() {
        $response = new RawGetPaginatedPokemonResponse();
        $this->assertEquals(1, $response->getTotalPages());

        $response->setTotalPages(5);

        $this->assertEquals(5, $response->getTotalPages());
    }


    public function testPageSetter() {
        $response = new RawGetPaginatedPokemonResponse();
        $this->assertEquals(1, $response->getPage());

        $response->setPage(3);
        $this->assertEquals(3, $response->getPage());
    }

    public function testToArray() {
        $response = new RawGetPaginatedPokemonResponse();

        $this->assertEquals([
            'data' => [],
            'meta' => [
                'page' => 1,
                'page_total' => 1,
                'total' => 0,
            ]
        ], $response->toArray());
    }

}