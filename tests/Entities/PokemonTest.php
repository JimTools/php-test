<?php

namespace Tests\Entities;

use PHPUnit\Framework\TestCase;
use Pokedex\Entities\Pokemon;
use Pokedex\Entities\Type;

class PokemonTest extends TestCase
{

    public function testConstructorSetsProperties()
    {
        $entity = new Pokemon(
            1,
            'bulbasaur',
            'bulbasaur',
            69,
            7,
            [],
            [
                new Type(2, 'Poison'),
                new Type(1, 'Grass'),
            ],
            'path/to/img.png'
        );

        $this->assertEquals(1, $entity->getId());
        $this->assertEquals('bulbasaur', $entity->getName());
        $this->assertEquals(69, $entity->getWeight());
        $this->assertEquals(7, $entity->getHeight());
        $this->assertEquals(
            [new Type(2, 'Poison'), new Type(1, 'Grass')],
            $entity->getTypes());
        $this->assertEquals('path/to/img.png', $entity->getImage());
    }

    public function testIdSetter()
    {
        $entity = new Pokemon(1, 'bulbasaur');
        $entity->setId(2);
        $this->assertEquals($entity->getId(), 2);
    }

    public function testNameSetter()
    {
        $entity = new Pokemon(1, 'bulbasaur');
        $entity->setName('fearow');
        $this->assertEquals($entity->getName(), 'fearow');
    }

    public function testWeightSetter()
    {
        $entity = new Pokemon(1, 'bulbasaur');

        $this->assertNull($entity->getWeight());
        $entity->setWeight(69);
        $this->assertEquals($entity->getWeight(), 69);
    }

    public function testHeighttSetter()
    {
        $entity = new Pokemon(1, 'bulbasaur');

        $this->assertNull($entity->getHeight());
        $entity->setWeight(7);
        $this->assertEquals($entity->getWeight(), 7);
    }

}