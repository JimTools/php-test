<?php

namespace Tests\Entities;

use PHPUnit\Framework\TestCase;
use Pokedex\Entities\Type;

class TypeTest extends TestCase
{

    public function testConstructorProperties() {
        $entity = new Type(1, 'Grass');

        $this->assertEquals(1, $entity->getId());
        $this->assertEquals('Grass', $entity->getName());
    }
    
    public function testIdSetter() {
        $entity = new Type(1, 'Grass');
        $entity->setId(2);

        $this->assertEquals(2, $entity->getId());

    }

    public function testNameSetter()
    {
        $entity = new Type(1, 'Grass');
        $entity->setName('Poison');

        $this->assertEquals('Poison', $entity->getName());


    }

}