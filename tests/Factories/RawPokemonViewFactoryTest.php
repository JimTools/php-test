<?php

namespace Tests\Factories;

use PHPUnit\Framework\TestCase;
use Pokedex\Entities\Pokemon;
use Pokedex\Entities\Type;
use Pokedex\Factories\RawPokemonViewFactory;
use Pokedex\Views\RawTypeView;

class RawPokemonViewFactoryTest extends TestCase
{

    public function testRawPokemonView()
    {

        $entity = new Pokemon(
            1,
            'bulbasaur',
            'bulbasaur',
            69,
            7,
            [],
            [new Type(2, 'poison'), new Type(1, 'grass')],
            'path/to/image'
        );
        $view = (new RawPokemonViewFactory())->create($entity);


        $this->assertEquals(1, $view->id);
        $this->assertEquals('bulbasaur', $view->name);
        $this->assertEquals(69, $view->weight);
        $this->assertEquals(7, $view->height);
        $this->assertEquals('path/to/image', $view->image);

        // types
        $this->assertEquals(2, $view->types[0]->id);
        $this->assertEquals('poison', $view->types[0]->name);

        $this->assertEquals(1, $view->types[1]->id);
        $this->assertEquals('grass', $view->types[1]->name);


    }

}
