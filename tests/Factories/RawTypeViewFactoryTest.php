<?php

namespace Tests\Factories;

use PHPUnit\Framework\TestCase;
use Pokedex\Entities\Type;
use Pokedex\Factories\RawTypeViewFactory;

class RawTypeViewFactoryTest extends TestCase
{

    public function testRawTypeViewFactory() {

        $entity = new Type(1, 'Grass');
        $view = (new RawTypeViewFactory())->create($entity);

        $this->assertEquals(1, $view->id);
        $this->assertEquals('Grass', $view->name);


    }

}
