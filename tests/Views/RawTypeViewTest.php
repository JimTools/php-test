<?php

namespace Tests\Views;

use PHPUnit\Framework\TestCase;
use Pokedex\Views\RawTypeView;

class RawTypeViewTest extends TestCase
{
    public function testProperties() {
        $view = new RawTypeView();
        $view->id = 1;
        $view->name = 'Grass';

        $this->assertEquals($view->id, 1);
        $this->assertEquals($view->name, 'Grass');
    }
}
