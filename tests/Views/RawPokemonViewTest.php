<?php

namespace Tests\Views;

use PHPUnit\Framework\TestCase;
use Pokedex\Views\RawPokemonView;
use Pokedex\Views\RawTypeView;


class RawPokemonViewTest extends TestCase
{

    public function testProperties()
    {
        $view = new RawPokemonView();

        $typeView = new RawTypeView();
        $typeView->id = 4;
        $typeView->name = 'Grass';
        $view->types = [$typeView];

        $view->id = 1;
        $view->name = 'bulbasaur';
        $view->weight = 69;
        $view->height = 7;
        $view->image = 'path/to/img.png';


        $this->assertEquals(4, $view->types[0]->id);
        $this->assertEquals('Grass', $view->types[0]->name);

//        $this->assertEquals(1, $view->id);
        $this->assertEquals('bulbasaur', $view->name);
        $this->assertEquals(69, $view->weight);
        $this->assertEquals(7, $view->height);
        $this->assertEquals('path/to/img.png', $view->image);


    }

}
