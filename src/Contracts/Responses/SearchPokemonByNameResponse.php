<?php

namespace Pokedex\Contracts\Responses;

use Pokedex\Contracts\Views\PokemonView;

interface SearchPokemonByNameResponse
{

    /**
     * @return PokemonView[]
     */
    public function getResults(): array;

    /**
     * @param PokemonView[] $results
     */
    public function setResults(array $results): void;

    /**
     * @return int
     */
    public function getCount(): int;

    /**
     * @param int $count
     */
    public function setCount(int $count): void;

}