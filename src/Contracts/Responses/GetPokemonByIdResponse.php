<?php

namespace Pokedex\Contracts\Responses;

use Pokedex\Contracts\Views\PokemonView;

interface GetPokemonByIdResponse
{
    /**
     * @return null|PokemonView
     */
    public function getPokemon(): ?PokemonView;

    /**
     * @param PokemonView $view
     */
    public function setPokemon(PokemonView $view): void;

    /**
     * @return null|string
     */
    public function getMessage(): ?string;

    /**
     * @param string $message
     */
    public function setMessage(string $message): void;

    /**
     * @return array
     */
    public function toArray(): array;
}