<?php

namespace Pokedex\Contracts\Responses;

use Pokedex\Contracts\Views\PokemonView;

interface GetPaginatedPokemonResponse
{

    /**
     * @return PokemonView[]
     */
    public function getResults(): array;

    /**
     * @param PokemonView[] $items
     */
    public function setResults(array $items): void;

    /**
     * get the total number of pokemon
     *
     * @return int
     */
    public function getTotalItems(): int;

    /**
     * set the total number of pokemon
     *
     * @param int $total
     */
    public function setTotalItems(int $total): void;

    /**
     * get the current page number
     *
     * @return int
     */
    public function getPage(): int;

    /**
     * sets the current page number
     *
     * @param int $page
     */
    public function setPage(int $page): void;

    /**
     * get the total number of pages
     *
     * @return int
     */
    public function getTotalPages(): int;

    /**
     * sets the total number of pages
     *
     * @param int $total
     */
    public function setTotalPages(int $total): void;

    /**
     * @return array
     */
    public function toArray(): array;

}