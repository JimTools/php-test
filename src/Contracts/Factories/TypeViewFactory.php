<?php

namespace Pokedex\Contracts\Factories;

use Pokedex\Contracts\Views\TypeView;
use Pokedex\Entities\Type;

interface TypeViewFactory
{
    public function create(Type $type): TypeView;

}