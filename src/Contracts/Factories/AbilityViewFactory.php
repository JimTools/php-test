<?php

namespace Pokedex\Contracts\Factories;

use Pokedex\Contracts\Views\AbilityView;
use Pokedex\Entities\Ability;

interface AbilityViewFactory
{
    public function create(Ability $ability): AbilityView;

}