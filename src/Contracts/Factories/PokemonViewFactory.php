<?php

namespace Pokedex\Contracts\Factories;

use Pokedex\Contracts\Views\PokemonView;
use Pokedex\Entities\Pokemon;

interface PokemonViewFactory
{

    /***
     * @param Pokemon $item
     * @return PokemonView
     */
    public function create(Pokemon $item): PokemonView;

}