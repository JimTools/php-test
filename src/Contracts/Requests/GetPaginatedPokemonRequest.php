<?php

namespace Pokedex\Contracts\Requests;


interface  GetPaginatedPokemonRequest
{

    public function __construct(int $page = 1, int $limit = 10);

    public function getLimit(): int;

    public function setLimit(int $limit): void;

    public function getPage(): int;

    public function setPage(int $page): void;

}