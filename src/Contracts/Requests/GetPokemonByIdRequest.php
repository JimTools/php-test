<?php

namespace Pokedex\Contracts\Requests;

interface GetPokemonByIdRequest
{

    /**
     * GetPokemonByIdRequest constructor.
     * @param int $id
     */
    public function __construct(int $id);

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     */
    public function setId(int $id): void;
}