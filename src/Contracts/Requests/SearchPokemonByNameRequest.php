<?php

namespace Pokedex\Contracts\Requests;

interface SearchPokemonByNameRequest
{

    /**
     * @return string
     */
    public function getTerm(): string;

    /**
     * @param string $term
     */
    public function setTerm(string $term): void;

}