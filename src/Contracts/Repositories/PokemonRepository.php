<?php


namespace Pokedex\Contracts\Repositories;

use Pokedex\Entities\Pokemon;

interface PokemonRepository
{
    /**
     * returns a list of pokemon based off limit and ofset
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPaginated(int $limit, int $offset): array;

    /**
     * return a single pokemon
     *
     * @param int $id
     * @return Pokemon
     */
    public function getPokemon(int $id): ?Pokemon;

    /**
     * search array of pokemon by search term
     *
     * @param string $term
     * @return Pokemon[]
     */
    public function findBySearch(string $term): array;

    /**
     * returns the total number of pokemon
     *
     * @return int
     */
    public function getCount(): int;
}