<?php

namespace Pokedex\Contracts\Views;

abstract class PokemonView
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $species;

    /**
     * @var int
     */
    public $weight;

    /**
     * @var int
     */
    public $height;

    /**
     * @var AbilityView[]
     */
    public $abilities;

    /**
     * @var TypeView[]
     */
    public $types;

    /**
     * @var string
     */
    public $image;
}