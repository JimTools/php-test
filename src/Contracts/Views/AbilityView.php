<?php

namespace Pokedex\Contracts\Views;

abstract class AbilityView
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string;
     */
    public $name;
}