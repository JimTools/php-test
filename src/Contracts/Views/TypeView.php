<?php

namespace Pokedex\Contracts\Views;

abstract class TypeView
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string;
     */
    public $name;
}