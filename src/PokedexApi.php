<?php

namespace Pokedex;

use Illuminate\Container\Container;
use Pokedex\Requests\RawGetPaginatedPokemonRequest;
use Pokedex\Requests\RawGetPokemonByIdRequest;
use Pokedex\Requests\RawSearchPokemonByNameRequest;
use Pokedex\Responses\RawGetPaginatedPokemonResponse;
use Pokedex\Responses\RawGetPokemonByIdResponse;
use Pokedex\Responses\RawSearchPokemonByNameResponse;
use Pokedex\UseCases\GetPaginatedPokemonUseCase;
use Pokedex\UseCases\GetPokemonByIdUseCase;
use Pokedex\UseCases\SearchPokemonByNameUseCase;

class PokedexApi
{
    /**
     * @var Container
     */
    private $container;

    /**
     * Api constructor.
     * @param array $repositories
     * @param array $factories
     */
    public function __construct(array $repositories = [], array $factories = [])
    {
        $this->container = Container::getInstance();

        $this->register($repositories);
        $this->register($factories);
    }


    /**
     * returns a list of pokemon
     *
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function index(int $page = 1, int $limit = 10): array
    {
        /**
         * @var $useCase GetPaginatedPokemonUseCase
         */
        $useCase = $this->container->make(GetPaginatedPokemonUseCase::class);

        $response = new RawGetPaginatedPokemonResponse();
        $useCase->process(new RawGetPaginatedPokemonRequest($page, $limit), $response);

        return $response->toArray();
    }

    /**
     * returns a
     *
     * @param int $id
     * @return array|Contracts\Views\PokemonView
     */
    public function show(int $id)
    {
        /**
         * @var $useCase GetPokemonByIdUseCase
         */
        $useCase = $this->container->make(GetPokemonByIdUseCase::class);

        $response = new RawGetPokemonByIdResponse();
        $useCase->process(new RawGetPokemonByIdRequest($id), $response);

        if (is_null($response->getPokemon())) {
            return ['message' => $response->getMessage()];
        }

        return $response->getPokemon();
    }

    /**
     * returns array of search results
     *
     * @param string $term
     * @return array
     */
    public function search(string $term): array
    {
        /**
         * @var $useCase SearchPokemonByNameUseCase
         */
        $useCase = $this->container->make(SearchPokemonByNameUseCase::class);
        $response = new RawSearchPokemonByNameResponse();

        $useCase->process(new RawSearchPokemonByNameRequest($term), $response);

        return $response->toArray();
    }

    /**
     * registers implementation to container
     *
     * @param array $config
     */
    private function register(array $config): void
    {
        foreach ($config as $contract => $concrete) {
            $this->container->singleton($contract, $concrete);
        }
    }
}