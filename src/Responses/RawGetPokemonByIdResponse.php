<?php

namespace Pokedex\Responses;


use Pokedex\Contracts\Responses\GetPokemonByIdResponse;
use Pokedex\Contracts\Views\PokemonView;

class RawGetPokemonByIdResponse implements GetPokemonByIdResponse
{

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var PokemonView|null
     */
    private $pokemon;

    /**
     * @return null|PokemonView
     */
    public function getPokemon(): ?PokemonView
    {
        return $this->pokemon;
    }

    /**
     * @param PokemonView $view
     */
    public function setPokemon(PokemonView $view): void
    {
        $this->pokemon = $view;
    }

    /**
     * @return null|string
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'message' => $this->message,
            'pokemon' => $this->pokemon,
        ];
    }
}