<?php

namespace Pokedex\Responses;

use Pokedex\Contracts\Responses\SearchPokemonByNameResponse;
use Pokedex\Contracts\Views\PokemonView;

class RawSearchPokemonByNameResponse implements SearchPokemonByNameResponse
{

    /**
     * @var PokemonView[]
     */
    private $results = [];

    /**
     * @var int
     */
    private $count = 0;

    /**
     * @return PokemonView[]
     */
    public function getResults(): array
    {
        return $this->results;
    }

    /**
     * @param array $results
     */
    public function setResults(array $results): void
    {
        $this->results = $results;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }

    public function toArray(): array
    {
        return [
            'data' => $this->getResults(),
            'meta' => [
                'count' => $this->getCount(),
            ],
        ];
    }
}