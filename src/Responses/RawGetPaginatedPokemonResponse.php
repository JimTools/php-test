<?php

namespace Pokedex\Responses;

use Pokedex\Contracts\Responses\GetPaginatedPokemonResponse;
use Pokedex\Contracts\Views\PokemonView;

class RawGetPaginatedPokemonResponse implements GetPaginatedPokemonResponse
{

    private $pokemon = [];

    private $totalItems = 0;

    private $page = 1;

    private $totalPages = 1;

    /**
     * @return PokemonView[]
     */
    public function getResults(): array
    {
        return $this->pokemon;
    }

    /**
     * @param PokemonView[] $items
     */
    public function setResults(array $items): void
    {
        $this->pokemon = $items;
    }

    /**
     * get the total number of pokemon
     *
     * @return int
     */
    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    /**
     * set the total number of pokemon
     *
     * @param int $total
     */
    public function setTotalItems(int $total): void
    {
        $this->totalItems = $total;
    }

    /**
     * get the current page number
     *
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * sets the current page number
     *
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }

    /**
     * get the total number of pages
     *
     * @return int
     */
    public function getTotalPages(): int
    {
        return $this->totalPages;
    }

    /**
     * sets the total number of pages
     *
     * @param int $total
     */
    public function setTotalPages(int $total): void
    {
        $this->totalPages = $total;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'data' => $this->getResults(),
            'meta' => [
                'page' => $this->getPage(),
                'total' => $this->getTotalItems(),
                'page_total' => $this->getTotalPages(),
            ]
        ];
    }
}