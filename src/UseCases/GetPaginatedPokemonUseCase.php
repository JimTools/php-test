<?php

namespace Pokedex\UseCases;

use Pokedex\Contracts\Factories\PokemonViewFactory;
use Pokedex\Contracts\Repositories\PokemonRepository;
use Pokedex\Contracts\Requests\GetPaginatedPokemonRequest;
use Pokedex\Contracts\Responses\GetPaginatedPokemonResponse;

class GetPaginatedPokemonUseCase
{

    /**
     * @var PokemonRepository
     */
    private $repository;

    /**
     * @var PokemonViewFactory
     */
    private $factory;

    /**
     * ListPokemonUseCase constructor.
     * @param PokemonRepository $repository
     * @param PokemonViewFactory $factory
     */
    public function __construct(PokemonRepository $repository, PokemonViewFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function process(GetPaginatedPokemonRequest $request, GetPaginatedPokemonResponse $response): void
    {
        // clamps lower limit
        $page = $request->getPage() < 1 ? 1 : $request->getPage();

        $results = $this->repository->getPaginated(
            $request->getLimit(),
            ($page === 1 ? 0 : $page -1) * $request->getLimit()
        );

        // claps the upper and lower limit of the page
        $totalItems = $this->repository->getCount();
        $totalPages = ceil($totalItems / $request->getLimit());


        $response->setResults(array_map(function ($item) {
            return $this->factory->create($item);
        }, $results));

        $response->setTotalPages((int)$totalPages);
        $response->setPage($page);
        $response->setTotalItems($totalItems);

    }

}