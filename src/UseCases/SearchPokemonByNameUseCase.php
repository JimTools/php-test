<?php

namespace Pokedex\UseCases;

use Pokedex\Contracts\Factories\PokemonViewFactory;
use Pokedex\Contracts\Repositories\PokemonRepository;
use Pokedex\Contracts\Requests\SearchPokemonByNameRequest;
use Pokedex\Contracts\Responses\SearchPokemonByNameResponse;
use Pokedex\Contracts\Views\PokemonView;
use Pokedex\Entities\Pokemon;

class SearchPokemonByNameUseCase
{

    /**
     * @var PokemonRepository
     */
    private $repository;

    /**
     * @var PokemonViewFactory
     */
    private $factory;

    /**
     * SearchPokemonByNameUseCase constructor.
     * @param PokemonRepository $repository
     * @param PokemonViewFactory $factory
     */
    public function __construct(PokemonRepository $repository, PokemonViewFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function process(SearchPokemonByNameRequest $request, SearchPokemonByNameResponse $response): void
    {

        $results = array_map(function (Pokemon $pokemon): PokemonView {
            return $this->factory->create($pokemon);
        }, $this->repository->findBySearch($request->getTerm()));

        $response->setResults($results);
        $response->setCount(count($results));
    }


}