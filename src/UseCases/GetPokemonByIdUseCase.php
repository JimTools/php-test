<?php

namespace Pokedex\UseCases;


use Pokedex\Contracts\Factories\PokemonViewFactory;
use Pokedex\Contracts\Repositories\PokemonRepository;
use Pokedex\Contracts\Requests\GetPokemonByIdRequest;
use Pokedex\Contracts\Responses\GetPokemonByIdResponse;

class GetPokemonByIdUseCase
{

    /**
     * @var PokemonRepository
     */
    private $repository;

    /**
     * @var PokemonViewFactory
     */
    private $factory;

    /**
     * GetPokemonByIdUseCase constructor.
     * @param PokemonRepository $repository
     * @param PokemonViewFactory $factory
     */
    public function __construct(PokemonRepository $repository, PokemonViewFactory $factory)
    {
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public function process(GetPokemonByIdRequest $request, GetPokemonByIdResponse $response): void
    {
        $pokemon = $this->repository->getPokemon($request->getId());
        if(is_null($pokemon)) {
            $response->setMessage('Unable to find pokemon.');
            return;
        }

        $response->setPokemon($this->factory->create($pokemon));

    }


}