<?php

namespace Pokedex\Requests;

use Pokedex\Contracts\Requests\GetPaginatedPokemonRequest;

class RawGetPaginatedPokemonRequest implements GetPaginatedPokemonRequest
{

    /**
     * @var int
     */
    private $page;

    /**
     * @var int
     */
    private $limit;

    public function __construct(int $page = 1, int $limit = 10)
    {
        $this->page = $page;
        $this->limit = $limit;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): void
    {
        $this->page = $page;
    }
}