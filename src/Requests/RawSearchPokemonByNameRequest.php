<?php

namespace Pokedex\Requests;

use Pokedex\Contracts\Requests\SearchPokemonByNameRequest;

class RawSearchPokemonByNameRequest implements SearchPokemonByNameRequest
{

    /**
     * @var string;
     */
    private $term = '';

    /**
     * RawSearchPokemonByNameRequest constructor.
     * @param string $term
     */
    public function __construct(string $term = '')
    {
        $this->term = $term;
    }


    /**
     * @return string
     */
    public function getTerm(): string
    {
        return $this->term;
    }

    /**
     * @param string $term
     */
    public function setTerm(string $term): void
    {
        $this->term = $term;
    }
}