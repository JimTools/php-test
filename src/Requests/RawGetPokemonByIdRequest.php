<?php

namespace Pokedex\Requests;

use Pokedex\Contracts\Requests\GetPokemonByIdRequest;

class RawGetPokemonByIdRequest implements GetPokemonByIdRequest
{

    /**
     * @var int
     */
    private $id;

    /**
     * GetPokemonByIdRequest constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}