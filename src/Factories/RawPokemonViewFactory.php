<?php

namespace Pokedex\Factories;

use Pokedex\Contracts\Factories\PokemonViewFactory;
use Pokedex\Contracts\Views\PokemonView;
use Pokedex\Entities\Pokemon;
use Pokedex\Views\RawAbilityView;
use Pokedex\Views\RawPokemonView;

class RawPokemonViewFactory implements PokemonViewFactory
{

    /***
     * @param Pokemon $item
     * @return PokemonView
     */
    public function create(Pokemon $item): PokemonView
    {
        $typeFactory = new RawTypeViewFactory();
        $abilityFactory = new RawAbilityViewFactory();

        $view = new RawPokemonView();
        $view->id = $item->getId();
        $view->name = $item->getName();
        $view->species = $item->getSpecies();
        $view->height = $item->getHeight();
        $view->weight = $item->getWeight();

        // abilities
        $view->abilities = array_map(function ($ability) use ($abilityFactory) {
            return $abilityFactory->create($ability);
        }, $item->getAbilities());

        // types
        $view->types = array_map(function ($type) use ($typeFactory) {
            return $typeFactory->create($type);
        }, $item->getTypes());
        $view->image = $item->getImage();

        return $view;
    }
}