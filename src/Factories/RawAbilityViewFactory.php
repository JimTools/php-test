<?php

namespace Pokedex\Factories;

use Pokedex\Contracts\Factories\AbilityViewFactory;
use Pokedex\Contracts\Views\AbilityView;
use Pokedex\Entities\Ability;
use Pokedex\Views\RawAbilityView;

class RawAbilityViewFactory implements AbilityViewFactory
{

    public function create(Ability $ability): AbilityView
    {
        $view = new RawAbilityView();
        $view->id = $ability->getId();
        $view->name = $ability->getName();
        return $view;
    }
}