<?php

namespace Pokedex\Factories;

use Pokedex\Contracts\Factories\TypeViewFactory;
use Pokedex\Contracts\Views\TypeView;
use Pokedex\Entities\Type;
use Pokedex\Views\RawTypeView;

class RawTypeViewFactory implements TypeViewFactory
{

    public function create(Type $type): TypeView
    {
        $view = new RawTypeView();
        $view->id = $type->getId();
        $view->name = $type->getName();
        return $view;
    }
}