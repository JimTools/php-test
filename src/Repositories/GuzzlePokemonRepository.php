<?php

namespace Pokedex\Repositories;

use Pokedex\Contracts\Repositories\PokemonRepository;
use Pokedex\Entities\Ability;
use Pokedex\Entities\Pokemon;
use GuzzleHttp\Client;
use Pokedex\Entities\Type;

class GuzzlePokemonRepository implements PokemonRepository
{

    /**
     * @var Client;
     */
    private $client;

    /**
     * @var int
     */
    private $count = 0;

    /**
     * GuzzlePokemonRepository constructor.
     */
    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'http://pokeapi.co/api/v2/',
        ]);
    }


    /**
     * returns a list of pokemon based off limit and ofset
     *
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getPaginated(int $limit, int $offset): array
    {
        $request = $this->client->get('pokemon/', [
            'query' => compact('limit', 'offset'),
        ]);

        $results = json_decode($request->getBody()->getContents());

        // sets total items
        $this->count = intval($results->count);


        // converts to entities
        return array_map(function($item) {
            // gets id from url
            preg_match('/(\d+)\/$/', $item->url, $match);

            return new Pokemon($match[1], $item->name);
        }, $results->results);



    }

    /**
     * return a single pokemon
     *
     * @param int $id
     * @return Pokemon
     */
    public function getPokemon(int $id): ?Pokemon
    {
        $request = $this->client->get('pokemon/' . $id);

        $body = json_decode($request->getBody()->getContents());

        return new Pokemon(
            $body->id,
            $body->name,
            $body->species->name,
            $body->weight,
            $body->height,
            array_map(function($ability) {
                preg_match('/(\d+)\/$/', $ability->ability->url, $match);
                return new Ability($match[1], $ability->ability->name);
            }, $body->abilities),
            array_map(function($type) {
                preg_match('/(\d+)\/$/', $type->type->url, $match);
                return new Type($match[1], $type->type->name);
            }, $body->types),
            $body->sprites->front_default
        );

        // 949
    }

    /**
     * search array of pokemon by search term
     *
     * @param string $term
     * @return Pokemon[]
     */
    public function findBySearch(string $term): array
    {
        $matches = [];

        $list = $this->getPaginated(949, 0);
        foreach($list as $pokemon) {
            similar_text($pokemon->getName(), $term, $percent);
            if($percent > 50) {
                $matches[] = $pokemon;
            }
        }

        return $matches;
    }

    /**
     * returns the total number of pokemon
     *
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}