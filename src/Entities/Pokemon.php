<?php

namespace Pokedex\Entities;

class Pokemon
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string;
     */
    private $species;
    /**
     * @var int|null
     */
    private $weight;

    /**
     * @var int|null
     */
    private $height;


    /**
     * @var Ability[]
     */
    private $abilities = [];

    /**
     * @var Type[]
     */
    private $types = [];

    /**
     * @var string|null
     */
    private $image;

    /**
     * Pokemon constructor.
     * @param int $id
     * @param string $name
     * @param string species
     * @param int $weight
     * @param int $height
     * @param array $abilities
     * @param Type[] $types
     * @param null|string $image
     * @internal param \string[] $typs
     */
    public function __construct(
        int $id,
        string $name,
        string $species = null,
        int $weight = null,
        $height = null,
        $abilities = [],
        $types = [],
        string $image = null
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->species = $species;
        $this->weight = $weight;
        $this->height = $height;
        $this->abilities = $abilities;
        $this->types = $types;
        $this->image = $image;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getWeight(): ?int
    {
        return $this->weight;
    }

    /**
     * @param int|null $weight
     */
    public function setWeight(?int $weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     */
    public function setHeight(?int $height): void
    {
        $this->height = $height;
    }

    /**
     * @return Type[]
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param Type[] $types
     */
    public function setTypes(array $types): void
    {
        $this->types = $types;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param null|string $image
     */
    public function setImage(?string $image): void
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getSpecies(): ?string
    {
        return $this->species;
    }

    /**
     * @param string $species
     */
    public function setSpecies(string $species)
    {
        $this->species = species;
    }

    /**
     * @return Ability[]
     */
    public function getAbilities(): array
    {
        return $this->abilities;
    }

    /**
     * @param Ability[] $abilities
     */
    public function setAbilities(array $abilities)
    {
        $this->abilities = $abilities;
    }






}